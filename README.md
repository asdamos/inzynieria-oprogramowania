IO
===
Inżynieria oprogramowania - przedmiot na UWr.

Pomysł
===
Aplikacja mobilna do nawigacji wewnątrz dużych obiektów
typu supermarket, galeria handlowa, dworzec czy lotnisko.

Prezentacja pomysłu: https://prezi.com/idk3fwvfa5cq/inzynieria-oprogramowania/

Twórcy
===
Adam Sawicki
Pior Szymajda
